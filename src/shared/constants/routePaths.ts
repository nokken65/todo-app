export const ROUTE_PATHS = {
  index: '/',
  settings: '/settings',
  feed: '/feed',
  signin: '/signin',
  signup: '/signup',
  comingsoon: '/coming_soon',
};
