import { ErrorFeedback } from './ErrorFeedback';
import { InputField } from './InputField';

export const Form = {
  InputField,
  ErrorFeedback,
};
